DROP DATABASE IF EXISTS HERO_DB;

CREATE DATABASE HERO_DB;

USE HERO_DB;

CREATE TABLE `Super` (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL
);

CREATE TABLE Power (
	id INT PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(255) NOT NULL
);

CREATE TABLE SuperPower (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    powerId INT NOT NULL,
    FOREIGN KEY (powerId) REFERENCES `Power`(id),
    PRIMARY KEY (superId, powerId)
);

CREATE TABLE Organization (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    phone VARCHAR(15) NOT NULL,
    email VARCHAR(255) NOT NULL
);

CREATE TABLE SuperOrganization (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    organizationId INT NOT NULL,
    FOREIGN KEY (organizationId) REFERENCES Organization(id),
    PRIMARY KEY (superId, organizationId)
);

CREATE TABLE Location (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    country CHAR(2) NOT NULL,
    state VARCHAR(255) NULL,
    city VARCHAR(255) NULL,
    streetAddress VARCHAR(255) NULL,
    latitudeDeg DECIMAL(9,7) NULL,
    longitudeDeg DECIMAL(10,7) NULL
);

CREATE TABLE OrganizationLocation (
	organizationId INT NOT NULL,
    FOREIGN KEY (organizationId) REFERENCES Organization(id),
    locationId INT NOT NULL,
    FOREIGN KEY (locationId) REFERENCES Location(id),
    PRIMARY KEY (organizationId, locationId)
);

CREATE TABLE Sighting (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `dateTime` DATETIME NOT NULL,
    locationId INT NOT NULL,
    FOREIGN KEY (locationId) REFERENCES Location(id)
);

CREATE TABLE SuperSighting (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    sightingId INT NOT NULL,
    FOREIGN KEY (sightingId) REFERENCES Sighting(id)
);

INSERT INTO `Super` (`name`, description)
VALUES ('Batman', 'Bruce Wayne'), -- 1
	('Wonder Woman', 'Princess Diana of Themyscira'), -- 2
    ('Mystique', 'Raven Darkholme'), -- 3
    ('Deadpool', 'Wade Winston Wilson'), -- 4
    ('Poison Ivy', 'Pamela Lillian Isley'), -- 5
    ('Doctor Strange', 'Stephen Vincent Strange'), -- 6
    ('Wolverine', 'James Howlett'); -- 7
    
INSERT INTO Power (description)
VALUES ('Intellect'), -- 1
	('Martial Arts'), -- 2
    ('Technology'), -- 3
    ('Strength'), -- 4
    ('Speed'), -- 5
    ('Flight'), -- 6
    ('Shapeshifting'), -- 7
    ('Agility'), -- 8
    ('Healing'), -- 9
    ('Poison Generation'), -- 10
    ('Toxin Immunity'), -- 11
    ('Magic'), -- 12
    ('Medicine'), -- 13
    ('Retractable Claws'), -- 14
    ('Energy Sourcing'),
    ('Chi'),
    ('Telekinesis'),
    ('Power Augmntation'),
    ('Power Bestowal'),
    ('Power Mimicry or Absorption'),
    ('Power Negation'),
    ('Power Sensing'),
    ('Acid Generation'),
    ('Animal Mimicry'),
    ('Biological Manipulation'),
    ('Body Part Substitution'),
    ('Bone Manipulation'),
    ('Duplication (Physical)'),
    ('Duplication (Temporal)'),
    ('Echolocation'),
    ('Firebreathing'),
    ('Healing Factor'),
    ('Invisibility'),
    ('Invulnerability'),
    ('Kinetic Absorption'),
    ('Superhuman Longevity'),
    ('Matter Injestion'),
    ('Merging'),
    ('Pheromone Manipulation'),
    ('Prehensile/Animated Hair'),
    ('Reactive Adaptation/Evolution'),
    ('Self-Detonation / Explosion & Reformation'),
    ('Sonic Scream'),
    ('Superhuman Durability / Endurance'),
    ('Superhuman Senses'),
    ('Night Vision'),
    ('X-Ray Vision'),
    ('Heat Vision'),
    ('Telescopic Vision'),
    ('Freeze Vision'),
    ('Vortex Breath'),
    ('Wallcrawling'),
    ('Waterbreathing'),
    ('Ecological Empathy'),
    ('Innate Capability'),
    ('Omni-linguism'),
    ('Omniscience'),
    ('Superhuman Tracking'),
    ('Astral Projection'),
    ('Cross-Dimensional Awareness'),
    ('Empathy'),
    ('Spiritual Mediumship'),
    ('Precognition'),
    ('Psychometry'),
    ('Telepathy'),
    ('Technopathy'),
    ('Astral Trapping'),
    ('Memory Manipulation'),
    ('Mind Control'),
    ('Possession'),
    ('Psionic Blast'),
    ('Psychic Weapons'),
    ('Animation'),
    ('Darkness / Shadow Manipulation'),
    ('Density Control'),
    ('Disintegration'),
    ('Elemental Transmutation'),
    ('Gravity Manipulation'),
    ('Immortality'),
    ('Intangibility / Phasing'),
    ('Light Manipulation'),
    ('Magnetism Manipulation'),
    ('Mass Manipulation'),
    ('Microwave Manipulation'),
    ('Molecular Manipulation'),
    ('Probability Manipulation'),
    ('Radiation Manipulation'),
    ('Reality Warping'),
    ('Resurrection'),
    ('Sound Manipulation'),
    ('Time Manipulation'),
    ('Air & Wind Manipulation'),
    ('Animal Control'),
    ('Cold & Ice Manipulation'),
    ('Earth & Stone Manipulation'),
    ('Electric Manipulation'),
    ('Fire & Heat Manipulation'),
    ('Plant Manipulation'),
    ('Water & Moisture Manipulation'),
    ('Weather Manipulation'),
    ('Concussion Beams'),
    ('Energy Blasts'),
    ('Energy Constructs'),
    ('Energy Conversion'),
    ('Force Field Generation'),
    ('Electrical Transportation'),
    ('Omnipresence'),
    ('Dimensional Travel'),
    ('Portal Creation'),
    ('Summoning'),
    ('Teleportation'),
    ('Time Travel'),
    ('Cosmic Energy Control'),
    ('Energy Aura Projection'),
    ('Magical Levitation'),
    ('Magnetic Levitation'),
    ('Sonic Repulsion Field'),
    ('Thermo-Chemical Energy'),
    ('Wings'),
    ('Illusion'),
    ('Animal Morphing'),
    ('Elasticity'),
    ('Inorganic'),
    ('Liquification'),
    ('Metamorphosis'),
    ('Sublimation'),
    ('Substance Mimicry');

INSERT INTO SuperPower (superId, powerId)
VALUES (1, 1), -- Batman, Intellect
	(1, 2), -- Batman, Martial Arts
    (1, 3), -- Batman, Technology
    (2, 2), -- Wonder Woman, Martial Arts
    (2, 4), -- Wonder Woman, Strength
    (2, 5), -- Wonder Woman, Speed
    (2, 6), -- Wonder Woman, Flight
    (3, 7), -- Mystique, Shapeshifting
    (3, 8), -- Mystique, Agility
    (3, 9), -- Mystique, Healing
    (4, 9), -- Deadpool, Healing
    (4, 2), -- Deadpool, Martial Arts
    (5, 10), -- Poison Ivy, Poison Generation
    (5, 11), -- Poison Ivy, Toxin Immunity
    (6, 12), -- Doctor Strange, Magic
    (6, 13), -- Doctor Strange, Medicine
    (6, 2), -- Doctor Strange, Martial Arts
    (7, 8), -- Wolverine, Agility
    (7, 9), -- Wolverine, Healing
    (7, 14), -- Wolverine, Retractable Claws
    (7, 2); -- Wolverine, Martial Arts
    
INSERT INTO Organization (`name`, description, phone, email)
VALUES ('Justice League', 'DC Comics', '468-569-5553', 'information@jla.org'), -- 1
	('Astonishing Avengers', 'Marvel Comics', '859-494-6564', 'astonishing@theavengers.com'), -- 2
    ('X-Men', 'Marvel Comics', '323-318-5364', 'inquiries@xmen.org'), -- 3
    ('Xavier Institute for Higher Learning', 'Marvel Comics', '803-928-5617', 'publicrelations@xavierinstitute.org'), -- 4
    ('Injustice League', 'DC Comics', '485-982-0868', 'info@injusticeleague.com'), -- 5
    ('Secret Society of Super Villains', 'DC Comics', '834-748-9388', 'contactus@ssosv.org'), -- 6
    ('Suicide Squad', 'DC Comics', '317-821-3132', 'contact@suicidesquad.com'), -- 7
    ('Avengers', 'Marvel Comics', '333-541-8467', 'generalinfo@theavengers.com'); -- 8
    
INSERT INTO SuperOrganization (superId, organizationId)
VALUES (1, 1), -- Batman, Justice League
	(2, 1), -- Wonder Woman, Justice League
    (3, 2), -- Mystique, Astonishing Avengesr
    (3, 3), -- Mystique, X-Men
    (3, 4), -- Mystique, Xavier Institute
    (4, 3), -- Deadpool, X-Men
    (5, 5), -- Poison Ivy, Injustice League
    (5, 6), -- Poison Ivy, SSoSV
    (5, 7), -- Poison Ivy, Suicide Squad
    (6, 8), -- Doctor Strange, Avengers
    (7, 3), -- Wolverine, X-Men
    (7, 8); -- Wolverine, Avengers
    
INSERT INTO Location (`name`, description, country, state, city, streetAddress, latitudeDeg, longitudeDeg)
VALUES
	-- Organization Locations
	('Justice League Watchtower', 'Justice League Headquarters', 'US', NULL, 'Moon', '100 Moonrock Boulevard', NULL, NULL), -- 1
	('Avengers Mansion', 'Avengers Headquarters', 'US', 'New York', 'New York City', '721 Fifth Avenue', NULL, NULL), -- 2
    ('X-Mansion', 'X-Corporation Headquarters', 'US', 'New York', 'North Salem', '1407 Graymalkin Lane', NULL, NULL), -- 3
    ('Swamp', 'Injustice League Headquarers', 'US', 'Florida', 'Tamarac', '2514 Travis Street', NULL, NULL), -- 4
    ('Clubhouse', 'Secret Society of Super Villains Headquarters', 'US', 'Hawaii', 'Maunaloa', '4530 Dogwood Lane', NULL, NULL), -- 5
    ('Belle Reve Penitentiary', 'Suicide Squad Headquarters', 'US', 'Louisiana', 'Terrebonne Parish', '92 Forest Lane', NULL, NULL), -- 6
    -- Sighting Locations
    ('Grande Mosquee de Heroumbili', 'North on RN 3', 'KM', NULL, 'Heroumbili', NULL, -11.5459875, 43.3885797), -- 7
    ('High Crossing Road', 'On 305 west of Rosanky', 'US', 'Texas', 'Smithville', '1 High Crossing Road', NULL, NULL), -- 8
    ('Loomis Circle', 'Near I-94 and Lien Road', 'US', 'Wisconsin', 'Madison', '3 Loomis Circle', NULL, NULL), -- 9
    ('Masjid Raqudatus Sholeh', 'Mosque', 'ID', 'West Java', 'Ambunten', NULL, -6.9011055, 113.7543578), -- 10
    ('Ergates', NULL, 'CY', NULL, 'Ergates', NULL, 35.057659, 33.2419422), -- 11
    ('Macheke Lodges and Conference Center', 'Just east on Mutare Road', 'ZI', NULL, 'Macheke', NULL, -18.1450471, 31.8477213), -- 12
    ('North East Township', NULL, 'US', 'Pennsylvania', 'North East', '12004 Kerr Rd', NULL, NULL), -- 13
    ('Mariner''s Inn', NULL, 'US', 'Wisconsin', 'Madison', '5339 Lighthouse Bay Dr', NULL, NULL), -- 14
    ('Remote Labinot-Mal', NULL, 'AL', NULL, 'Labinot-Mal', NULL, 41.1823974, 20.15515), -- 15
    ('Oak Valley Road', 'Down the road from P.J. Julius Rod Company', 'US', 'Wisconsin', 'Cross Plains', '9 Oak Valley Road', NULL, NULL), -- 16
    ('Hunters Edge Neighborhood', 'Near the pharmacy', 'US', 'Indiana', 'Greenwood', '87453 Sloan Terrace', NULL, NULL), -- 17
    ('Rafael Castillo', 'Chavarria 3098', 'AR', NULL, 'Buenos Aires', NULL, -34.7121944, -58.6285183), -- 18
    ('Funing Flavor Snack Bar', NULL, 'CN', 'Fujian', 'Ningde', NULL, 26.8856364, 120.0040054), -- 19
    ('Scoville Ave', 'Right outside Berwyn Recreation Department', 'US', 'Illinois', 'Berwyn', '91180 Scoville Park', NULL, NULL), -- 20
    ('Artisan Parkway', 'Down the road from Covenant United Methodist Church', 'US', 'Kentucky', 'La Grange', '2 Artisan Parkway', NULL, NULL), -- 21
    ('Claremont Avenue', 'Just south of Crescent Hill Golf Course', 'US', 'Kentucky', 'Louisville', '529 Claremont Avenue', NULL, NULL), -- 22
    ('School Street', 'North of Main Street Meats', 'US', 'Indiana', 'New Pekin', '83561 School Street', NULL, NULL), -- 23
    ('Casa Vacanza San Nicolosio', NULL, 'IT', NULL, 'Genova', NULL, 44.4133103, 8.9314338), -- 24
    ('Kamo', 'North of Kamo Post Office', 'JP', NULL, 'Kamo', NULL, 38.7607275, 139.7347818), -- 25
    ('Prairie Rose Court', 'Northwest of The Glenwood', 'US', 'Illinois', 'Mount Zion', '99 Prairie Rose Court', NULL, NULL), -- 26
    ('Masjid Jami Paojan', 'Mosque', 'ID', 'West Java', 'Situwangi', NULL, -6.9559495, 107.5099087), -- 27
    ('Jin He Lu', NULL, 'CN', 'Sichuan', 'Panzhihua', NULL, 26.695419, 101.841707), -- 28
    ('North Avenue', 'Northeast of the US Army Reserve', 'US', 'Wisconsin', 'Junction City', '84 North Avenue', NULL, NULL), -- 29
    ('Nam Som', 'Nam Som District', 'TH', 'Udon Thani', 'Nam Som', NULL, 17.7177253, 102.1175404), -- 30
    ('Alpine Trail', NULL, 'US', 'Wisconsin', 'Wautoma', '4 Alpine Trail', NULL, NULL), -- 31
    ('Amroth Lane', 'Field', 'US', 'Wisconsin', 'Ettrick', '27 Amoth Lane', NULL, NULL), -- 32
    ('Creteil Soleil', 'Shopping Mall', 'FR', NULL, 'Creteil', '28 Avenue de la France libre', 48.7803815, 2.4549884); -- 33
    
INSERT INTO OrganizationLocation (organizationId, locationId)
VALUES (1, 1), -- Justice League, Justice League Watchtower
	(2, 2), -- Astonishing Avengers, Avengers Mansion
    (3, 3), -- X-Men, X-Mansion
    (4, 3), -- Xavier Institute for Higher Learning, X-Mansion
    (5, 4), -- Injustice League, Swamp
    (6, 5), -- SSoSV, Clubhouse
    (7, 6), -- Suicide Squad, Belle Reve Penitentiary
    (8, 2); -- Avengers, Avengers Mansion
    
INSERT INTO Sighting (`dateTime`, locationId)
VALUES ('2015-06-08 15:14:47', 7), -- 1
	('2017-06-03 23:22:41', 5), -- 2
	('2015-04-16 03:24:27', 8), -- 3
	('2015-03-18 13:14:21', 9), -- 4
	('2016-05-06 09:54:43', 10), -- 5
	('2015-08-28 19:22:39', 4), -- 6
	('2015-02-28 15:51:56', 11), -- 7
	('2017-02-27 16:54:21', 12), -- 8
	('2015-12-02 05:15:45', 13), -- 9
	('2017-09-14 11:46:30', 14), -- 10
	('2016-03-03 07:09:53', 15), -- 11
	('2015-12-22 03:40:50', 16), -- 12
	('2014-11-29 03:00:11', 17), -- 13
	('2016-06-09 22:12:02', 18), -- 14
	('2016-08-28 04:06:49', 19), -- 15
	('2016-07-30 22:33:17', 20), -- 16
	('2017-05-31 03:50:10', 6), -- 17
	('2015-03-07 05:40:05', 21), -- 18
	('2017-01-11 11:37:07', 22), -- 19
	('2017-04-14 10:36:14', 3), -- 20
	('2017-03-07 16:36:51', 23), -- 21
	('2017-03-09 05:38:58', 24), -- 22
	('2015-11-12 01:15:59', 25), -- 23
	('2017-02-15 09:58:52', 26), -- 24
	('2017-10-16 03:39:03', 27), -- 25
	('2017-06-07 04:03:13', 28), -- 26
	('2016-08-16 15:44:05', 29), -- 27
    ('2015-08-15 04:24:41', 30), -- 28
	('2017-05-25 02:57:11', 31), -- 29
	('2017-06-11 11:51:00', 32), -- 30
	('2016-01-22 13:19:29', 33), -- 31
	('2017-03-06 02:23:23', 4), -- 32
	('2015-05-29 13:42:32', 3), -- 33
	('2017-02-07 14:16:22', 24), -- 34
	('2015-01-16 04:33:16', 33), -- 35
	('2014-12-11 10:49:01', 17), -- 36
	('2017-07-02 16:14:07', 2), -- 37
	('2015-09-30 06:14:57', 8), -- 38
	('2017-03-24 12:20:07', 33), -- 39
	('2015-07-21 13:29:25', 19), -- 40
	('2015-02-02 05:37:50', 5), -- 41
	('2014-12-27 18:11:05', 9), -- 42
    ('2016-03-23 03:01:42', 4), -- 43
	('2016-09-20 17:27:54', 20), -- 44
	('2017-01-22 00:00:22', 9), -- 45
	('2016-06-20 02:02:10', 10), -- 46
	('2015-09-16 01:44:35', 23), -- 47
	('2016-05-28 15:29:07', 29), -- 48
	('2015-06-28 17:22:55', 23), -- 49
	('2015-09-30 10:21:42', 12), -- 50
	('2014-11-25 17:46:05', 22), -- 51
	('2015-06-30 03:29:08', 18), -- 52
	('2015-06-21 09:43:28', 7), -- 53
	('2017-04-09 17:16:37', 19), -- 54
	('2016-04-29 10:40:03', 2), -- 55
	('2016-10-27 22:48:05', 32), -- 56
	('2015-02-06 15:02:00', 26), -- 57
	('2016-05-20 23:22:33', 31), -- 58
	('2016-07-06 13:53:20', 31), -- 59
	('2017-08-25 00:09:55', 26); -- 60
	
INSERT INTO SuperSighting (superId, sightingId)
VALUES (2, 1),
	(3, 1),
	(2, 2),
	(4, 3),
	(5, 3),
    (3, 4),
	(1, 5),
	(4, 6),
    (7, 6),
	(4, 7),
    (5, 7),
	(3, 8),
	(4, 9),
	(5, 10),
	(4, 11),
	(2, 12),
    (5, 12),
	(6, 12),
    (2, 13),
	(5, 13),
    (3, 14),
	(4, 14),
    (1, 15),
	(3, 15),
    (5, 15),
    (5, 16),
	(6, 16),
    (2, 17),
	(2, 18),
    (3, 18),
	(3, 19),
	(4, 20),
	(6, 21),
	(1, 22),
	(7, 23),
	(6, 24),
	(1, 25),
	(5, 26),
	(7, 27),
	(2, 28),
	(5, 28),
    (6, 28),
    (2, 29),
	(4, 29),
    (6, 29),
    (1, 30),
	(7, 30),
    (4, 31),
	(7, 31),
    (2, 32),
	(4, 33),
    (5, 33),
	(2, 34),
	(1, 35),
    (2, 35),
	(6, 35),
    (2, 36),
	(5, 37),
	(3, 38),
    (6, 38),
	(1, 39),
    (3, 39),
    (7, 39),
	(2, 40),
	(4, 41),
	(5, 42),
	(6, 42),
    (1, 43),
	(3, 44),
	(7, 44),
    (2, 45),
    (3, 45),
	(2, 46),
	(3, 47),
    (4, 47),
	(6, 47),
    (3, 48),
    (7, 48),
	(3, 49),
	(5, 49),
    (4, 50),
	(1, 51),
	(3, 52),
	(2, 53),
	(6, 54),
	(7, 54),
    (3, 55),
    (4, 55),
	(4, 56),
	(7, 57),
	(4, 58),
	(5, 58),
    (6, 59),
	(2, 60);