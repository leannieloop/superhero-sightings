DROP DATABASE IF EXISTS HERO_DB_test;

CREATE DATABASE HERO_DB_test;

USE HERO_DB_test;

CREATE TABLE `Super` (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL
);

CREATE TABLE Power (
	id INT PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(255) NOT NULL
);

CREATE TABLE SuperPower (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    powerId INT NOT NULL,
    FOREIGN KEY (powerId) REFERENCES `Power`(id),
    PRIMARY KEY (superId, powerId)
);

CREATE TABLE Organization (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    phone VARCHAR(15) NOT NULL,
    email VARCHAR(255) NOT NULL
);

CREATE TABLE SuperOrganization (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    organizationId INT NOT NULL,
    FOREIGN KEY (organizationId) REFERENCES Organization(id),
    PRIMARY KEY (superId, organizationId)
);

CREATE TABLE Location (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    country CHAR(2) NOT NULL,
    state VARCHAR(255) NULL,
    city VARCHAR(255) NULL,
    streetAddress VARCHAR(255) NULL,
    latitudeDeg DECIMAL(9,7) NULL,
    longitudeDeg DECIMAL(10,7) NULL
);

CREATE TABLE OrganizationLocation (
	organizationId INT NOT NULL,
    FOREIGN KEY (organizationId) REFERENCES Organization(id),
    locationId INT NOT NULL,
    FOREIGN KEY (locationId) REFERENCES Location(id),
    PRIMARY KEY (organizationId, locationId)
);

CREATE TABLE Sighting (
	id INT PRIMARY KEY AUTO_INCREMENT,
    `dateTime` DATETIME NOT NULL,
    locationId INT NOT NULL,
    FOREIGN KEY (locationId) REFERENCES Location(id)
);

CREATE TABLE SuperSighting (
	superId INT NOT NULL,
    FOREIGN KEY (superId) REFERENCES `Super`(id),
    sightingId INT NOT NULL,
    FOREIGN KEY (sightingId) REFERENCES Sighting(id)
);

INSERT INTO `Super` (`name`, description)
VALUES ('Default Super', 'Default Alter Ego'); -- 1

INSERT INTO Power (description)
VALUES ('Default Power'); -- 1

INSERT INTO SuperPower (superId, powerId)
VALUES (1, 1); -- Default Super, Default Power
    
INSERT INTO Organization (`name`, description, phone, email)
VALUES ('Default Organization', 'Default Description', '123-456-7890', 'default@email.com'); -- 1
    
INSERT INTO SuperOrganization (superId, organizationId)
VALUES (1, 1); -- Default Super, Default Organization
    
INSERT INTO Location (`name`, description, country, state, city, streetAddress, latitudeDeg, longitudeDeg)
VALUES ('Default Location', 'Default Description', 'NA', 'Default State', 'Default City', 'Default Address', 12.3456789, 12.34567890); -- 1
    
INSERT INTO OrganizationLocation (organizationId, locationId)
VALUES (1, 1); -- Default Organization, Default Location
    
INSERT INTO Sighting (`dateTime`, locationId)
VALUES ('1000-01-01 00:00:00', 1); -- 1
	
INSERT INTO SuperSighting (superId, sightingId)
VALUES (1, 1); -- Default Super, Default Sighting