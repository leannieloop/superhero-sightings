package com.sg.superherosightings.controller;

import com.sg.superherosightings.model.Power;
import com.sg.superherosightings.service.Service;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@CrossOrigin
@Controller
public class PowerRestController {

    @Inject
    private Service service;

    @RequestMapping(value = "/power", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void addPower(@RequestBody Power power) {
        service.addPower(power);
    }

    @RequestMapping(value = "/power/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Power getPower(@PathVariable("id") Integer id) {
        return service.getPowerById(id);
    }

    @RequestMapping(value = "/powers", method = RequestMethod.GET)
    @ResponseBody
    public List<Power> getAllPowers() {
        return service.getAllPowers();
    }

    @RequestMapping(value = "/power/{id}", method = RequestMethod.PATCH)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePower(@RequestBody Power power) {
        service.updatePower(power);
    }

    @RequestMapping(value = "/power/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePower(@PathVariable("id") Integer id) {
        service.deletePower(id);
    }
}
