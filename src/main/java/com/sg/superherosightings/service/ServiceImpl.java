package com.sg.superherosightings.service;

import com.sg.superherosightings.dao.Builder;
import com.sg.superherosightings.dao.LocationDao;
import com.sg.superherosightings.dao.OrganizationDao;
import com.sg.superherosightings.dao.PowerDao;
import com.sg.superherosightings.dao.SightingDao;
import com.sg.superherosightings.dao.SuperDao;
import com.sg.superherosightings.model.Location;
import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Power;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceImpl implements Service {

    private SuperDao superDao;
    private PowerDao powerDao;
    private LocationDao locationDao;
    private OrganizationDao organizationDao;
    private SightingDao sightingDao;
    private Builder builder;

    public ServiceImpl(SuperDao superDao, PowerDao powerDao,
            LocationDao locationDao, OrganizationDao organizationDao,
            SightingDao sightingDao, Builder builder) {
        this.superDao = superDao;
        this.powerDao = powerDao;
        this.locationDao = locationDao;
        this.organizationDao = organizationDao;
        this.sightingDao = sightingDao;
        this.builder = builder;
    }

    @Override
    public void addSuper(Super superPerson) {
        superDao.addSuper(superPerson);
    }

    @Override
    public Super getSuperById(Integer superId) {
        return builder.buildCompleteSuper(superDao.getSuperById(superId));
    }

    @Override
    public List<Super> getAllSupers() {
        return getCompleteSupers(superDao.getAllSupers());
    }

    @Override
    public List<Super> getSupersBySightingLocation(Integer locationId) {
        return getCompleteSupers(superDao.getSupersBySightingLocation(locationId));
    }

    @Override
    public void updateSuper(Super superPerson) {
        superDao.updateSuper(superPerson);
    }

    @Override
    public void deleteSuper(Integer superId) {
        superDao.deleteSuper(superId);
    }

    private List<Super> getCompleteSupers(List<Super> supers) {
        for (Super superPerson : supers) {
            superPerson = builder.buildCompleteSuper(superPerson);
        }
        return supers;
    }

    @Override
    public void addPower(Power power) {
        powerDao.addPower(power);
    }

    @Override
    public Power getPowerById(Integer powerId) {
        return powerDao.getPowerById(powerId);
    }

    @Override
    public List<Power> getAllPowers() {
        return powerDao.getAllPowers()
                .stream()
                .sorted(Comparator.comparing(Power::getDescription))
                .collect(Collectors.toList());
    }

    @Override
    public void updatePower(Power power) {
        powerDao.updatePower(power);
    }

    @Override
    public void deletePower(Integer powerId) {
        powerDao.deletePower(powerId);
    }

    @Override
    public void addLocation(Location location) {
        locationDao.addLocation(location);
    }

    @Override
    public Location getLocationById(Integer locationId) {
        return locationDao.getLocationById(locationId);
    }

    @Override
    public List<Location> getAllLocations() {
        return locationDao.getAllLocations();
    }

    @Override
    public List<Location> getLocationsWhereSuperHasBeenSighted(Integer superId) {
        return locationDao.getLocationsWhereSuperHasBeenSighted(superId);
    }

    @Override
    public void updateLocation(Location location) {
        locationDao.updateLocation(location);
    }

    @Override
    public void deleteLocation(Integer locationId) {
        locationDao.deleteLocation(locationId);
    }

    @Override
    public void addOrganization(Organization organization) {
        organizationDao.addOrganization(organization);
    }

    @Override
    public Organization getOrganizationById(Integer organizationId) {
        return builder.buildCompleteOrganization(
                organizationDao.getOrganizationById(organizationId));
    }

    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> organizations = organizationDao.getAllOrganizations()
                .stream()
                .sorted(Comparator.comparing(Organization::getName))
                .collect(Collectors.toList());
        organizations.forEach(o -> {
            o = builder.buildCompleteOrganization(o);
        });
        return organizations;
    }

    @Override
    public void updateOrganization(Organization organization) {
        organizationDao.updateOrganization(organization);
    }

    @Override
    public void deleteOrganization(Integer organizationId) {
        organizationDao.deleteOrganization(organizationId);
    }

    @Override
    public void addSighting(Sighting sighting) {
        sightingDao.addSighting(sighting);
    }

    @Override
    public Sighting getSightingById(Integer sightingId) {
        return builder.buildCompleteSighting(
                sightingDao.getSightingById(sightingId));
    }

    @Override
    public List<Sighting> getAllSightings() {
        List<Sighting> sightings = new ArrayList<>();
        sightingDao.getAllSightings()
                .stream()
                .sorted(Comparator.comparing(Sighting::getDateTime))
                .collect(Collectors.toList())
                .forEach(s -> sightings.add(0, s));
        return getCompleteSightings(sightings);
    }

    @Override
    public List<Sighting> getRecentSightings() {
        List<Sighting> lastTenSightings = getAllSightings();
        while (lastTenSightings.size() > 10) {
            lastTenSightings.remove(10);
        };
        return lastTenSightings;
    }

    @Override
    public List<Sighting> getSightingsByDate(Long dateInMilliseconds) {
        LocalDate date = Instant.ofEpochMilli(dateInMilliseconds)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        List<Sighting> sightings = new ArrayList<>();
        sightingDao.getSightingsByDate(date)
                .stream()
                .sorted(Comparator.comparing(Sighting::getDateTime))
                .collect(Collectors.toList())
                .forEach(s -> sightings.add(0, s));
        return getCompleteSightings(sightings);
    }

    @Override
    public List<Sighting> getSightingsBySuper(Integer superId) {
        List<Sighting> sightings = new ArrayList<>();
        sightingDao.getSightingsBySuper(superId)
                .stream()
                .sorted(Comparator.comparing(Sighting::getDateTime))
                .collect(Collectors.toList())
                .forEach(s -> sightings.add(0, s));
        return getCompleteSightings(sightings);
    }

    @Override
    public void updateSighting(Sighting sighting) {
        sightingDao.updateSighting(sighting);
    }

    @Override
    public void deleteSighting(Integer sightingId) {
        sightingDao.deleteSighting(sightingId);
    }

    private List<Sighting> getCompleteSightings(List<Sighting> sightings) {
        for (Sighting sighting : sightings) {
            sighting = builder.buildCompleteSighting(sighting);
        }
        return sightings;
    }
}
