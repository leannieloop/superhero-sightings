$(document).ready(function() {
    loadOrganizations();
    loadLocations();
    loadSupers();
    $('#org-list').on('click', 'div.default', function() {
        var id = $(this).attr('id');
        $('#org-list div#' + id + 'exp').slideToggle('slow');
        var expandArrow = $(this).children('div.arrow').children('i');
        if (expandArrow.text() === 'expand_more') {
            expandArrow.text('expand_less');
        } else if (expandArrow.text() === 'expand_less') {
            expandArrow.text('expand_more');
        }
    });
    $('#show-more-locations-btn').click(function() {
        showNextTenLocations();
    });
    $('#new-org-form').on('submit', function() {
        var isValid = false;
        if (!$('#name').val().trim()) {
            alert('Enter an organization name.');
        } else if (!$('#phone').val().trim()) {
            alert('Enter a phone number.');
        } else if (!$('#email').val().trim()) {
            alert('Enter a valid email address.');
        } else {
            isValid = true;
        }
        if (isValid) {
            addNewOrganization();
            return false;
        }
    });
    $('#org-list').on('click', 'div.edit', function() {
        fillEditForm($(this).attr('id').substring(4));
    });
    $('#cancel-edit-org').click(function() {
        clearEditForm();
    });
    $('#submit-edit-org').click(function() {
        editOrganization();
    });
    $('#org-list').on('click', 'div.delete', function() {
        if (window.confirm('Are you sure you want to delete this organization?')) {
            deleteOrganization($(this).attr('id').substring(6));
        } else {
            alert('Delete cancelled.');
        }
    });
});
function loadOrganizations() {
    $('#org-list').empty();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/organizations',
        success: function(data, status) {
            $.each(data, function(index, organization) {
                var id = organization.id;
                $('#org-list').append('<div class="edit" id="edit' + id + '">'
                        + '<a href="#" data-toggle="modal" data-target="#edit-org-modal" id="edit-org-modal-btn">'
                        + '<i class="material-icons">edit</i></a></div>'
                        + '<div class="delete" id="delete' + id + '">'
                        + '<a href="#" id="delete-master-org">'
                        + '<i class="material-icons">delete_forever</i></a></div>'
                        + '<div class="default" id="' + id + '"></div>');
                if (organization.description.length !== 0) {
                    $('#org-list div#' + id).append('<h6>' + organization.name
                            + ' (' + organization.description + ')');
                } else {
                    $('#org-list div#' + id).append('<h6>' + organization.name);
                }
                $('#org-list div#' + id).append('<p>'
                        + '<i class="material-icons">phone</i>&nbsp;'
                        + organization.phone + '</p>'
                        + '<p><i class="material-icons">email</i>&nbsp;'
                        + organization.email + '</p>');
                if (organization.locations.length !== 0 || organization.supers.length !== 0) {
                    $('#org-list div#' + id).append('<div class="expand" id="' + id
                            + 'exp"></div><div class="arrow">'
                            + '<i id="arrow" class="material-icons">expand_more</i></div>')
                    $.each(organization.locations, function(i, location) {
                        $('#org-list div#' + id + 'exp').append('<p>'
                                + '<i class="material-icons">map</i>&nbsp;'
                                + location.name + '</p>');
                        if (location.description != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;('
                                    + location.description + ')</p>');
                        }
                        ;
                        if (location.streetAddress != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;Address: '
                                    + location.streetAddress + '</p>');
                        }
                        ;
                        if (location.city != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;City: '
                                    + location.city + '</p>');
                        }
                        ;
                        if (location.state != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;State: '
                                    + location.state + '</p>');
                        }
                        ;
                        $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;Country: '
                                + location.country + '</p>');
                        if (location.latitude != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;Latitude: '
                                    + location.latitudeDeg + '</p>');
                        }
                        ;
                        if (location.longitude != null) {
                            $('#org-list div#' + id + 'exp').append('<p>&emsp;&emsp;Longitude: '
                                    + location.longitudeDeg + '</p>');
                        }
                        ;
                    });
                    $.each(organization.supers, function(i, member) {
                        $('#org-list div#' + id + 'exp').append('<p>'
                                + '<i class="material-icons">person</i>' + member.name
                                + ' (' + member.description + ')</p>');
                    });
                }
            });
        },
        error: function() {
            $('#load-error').show();
        }
    });
}

var numberOfLocations = 0;
var visibleLocations = 0;
function loadLocations() {
    $('#all-locations-list').empty();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/locations',
        success: function(data, status) {
            $.each(data, function(index, location) {
                numberOfLocations++;
                var id = location.id;
                $('#all-locations-list').append(
                        '<p class="location" id="' + id + '"></p>');
                var locationString = location.name;
                if (location.description != null) {
                    locationString += ' (' + location.description + ')';
                }
                if (location.streetAddress != null) {
                    locationString += ', ' + location.streetAddress;
                }
                if (location.city != null) {
                    locationString += ', ' + location.city;
                }
                if (location.state != null) {
                    locationString += ', ' + location.state;
                }
                locationString += ', ' + location.country;
                if (location.latitudeDeg != null && location.longitudeDeg != null) {
                    locationString += '; geo: '
                            + location.latitudeDeg + ',' + location.longitudeDeg;
                }
                $('#all-locations-list p#' + id).append('<input type="checkbox" id="location'
                        + id + '" value="' + id + '"/><label for="location'
                        + id + '">' + locationString + '</label>');
                $('#all-locations-list p#' + id).addClass('hidden');
            });
            showNextTenLocations();
        },
        error: function() {
            $('#all-locations-load-error').show();
        }
    });
}

function showNextTenLocations() {
    if (Number(numberOfLocations - visibleLocations) > 10) {
        var count = 0;
        $.each($('#all-locations-list p.location'), function() {
            if (count < 10) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                    count++;
                    visibleLocations++;
                }
            }
        });
        if (Number(numberOfLocations - visibleLocations) > 0) {
            $('#show-more-locations-btn').show();
        } else {
            $('#show-more-locations-btn').hide();
        }
    } else {
        $.each($('#all-locations-list div.location'), function() {
            if ($(this).hasClass('hidden')) {
                $(this).removeClass('hidden');
                visibleLocations++;
            }
        });
        $('#show-more-locations-btn').hide();
    }
}

function loadSupers() {
    $('#super-select').empty().html('<option value="" disabled>Select Super(s)</option>');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/supers',
        success: function(data, status) {
            var supers = {};
            $.each(data, function(index, superPerson) {
                var id = superPerson.id;
                var name = superPerson.name;
                $('#super-select').append('<option value="' + id + '">'
                        + name + ' (' + superPerson.description + ')</option>');
                $('#super-select').material_select();
            });
        },
        error: function() {
            $('#supers-load-error').show();
        }
    });
}

function addNewOrganization() {
    var jsonSupers = [];
    $('#super-select option:selected').each(function() {
        jsonSupers.push({id: Number($(this).val())});
    })
    var jsonLocations = [];
    $('#all-locations-list input:checked').each(function() {
        jsonLocations.push({id: Number($(this).val())});
    });
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/SuperheroSightings/organization',
        data: JSON.stringify({
            name: $('#name').val(),
            description: $('#description').val(),
            phone: $('#phone').val(),
            email: $('#email').val(),
            locations: jsonLocations,
            supers: jsonSupers
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#add-org-error').hide();
            $('#add-org-success').show();
            loadOrganizations();
            $('#new-org-form')[0].reset();
        },
        error: function() {
            $('#add-org-success').hide();
            $('#add-org-error').show();
        }
    });
}

function fillEditForm(id) {
    $('div.form-group h5').text('Edit Oranization');
    $('#submit-button').hide();
    $('.edit-buttons').show();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/SuperheroSightings/organization/' + id,
        success: function(org, status) {
            $('#org-id').val(id);
            $('#name').val(org.name);
            $('#description').val(org.description);
            $('#phone').val(org.phone);
            $('#email').val(org.email);
            var members = [];
            $.each(org.supers, function(index, member) {
                members.push(member.id);
            });
            $('#super-select').val(members);
            $('#super-select').material_select();
            $.each(org.locations, function(index, location) {
                $('#all-locations-list input#location' + location.id).attr('checked', 'checked');
            });
        },
        error: function() {
            alert('ERROR: Unable to populate the edit form.');
        }
    });
}

function editOrganization() {
    var jsonSupers = [];
    $('#super-select option:selected').each(function() {
        jsonSupers.push({id: Number($(this).val())});
    });
    var jsonLocations = [];
    $('#all-locations-list input:checked').each(function() {
        jsonLocations.push({id: Number($(this).val())});
    });
    $.ajax({
        type: 'PATCH',
        url: 'http://localhost:8080/SuperheroSightings/organization/' + $('#org-id').val(),
        data: JSON.stringify({
            id: Number($('#org-id').val()),
            name: $('#name').val(),
            description: $('#description').val(),
            phone: $('#phone').val(),
            email: $('#email').val(),
            locations: jsonLocations,
            supers: jsonSupers
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function() {
            $('#edit-org-error').hide();
            $('#edit-org-success').show();
            loadOrganizations();
            clearEditForm();
        },
        error: function() {
            $('#edit-org-success').hide();
            $('#edit-org-error').show();
        }
    });
}

function clearEditForm() {
    $('div.form-group form')[0].reset();
    $('#super-select').material_select();
    $('div.form-group h5').text('Add New Organization');
    $('.edit-buttons').hide();
    $('#submit-button').show();
}

function deleteOrganization(id) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/SuperheroSightings/organization/' + id,
        success: function() {
            loadOrganizations();
        },
        error: function() {
            alert('ERROR: Unable to delete organization.');
        }
    });
}