package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import com.sg.superherosightings.model.Sighting;
import com.sg.superherosightings.model.Super;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BuilderImplTest {

    private Builder builder;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        builder = ctx.getBean("builder", Builder.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testBuildCompleteSuper() {
        Super knownSuper = new Super();
        knownSuper.setId(1);
        knownSuper.setName("Default Super");
        knownSuper.setDescription("Default Alter Ego");
        knownSuper = builder.buildCompleteSuper(knownSuper);
        assertEquals(1, knownSuper.getPowers().size());
        assertEquals(1, knownSuper.getOrganizations().size());
        assertEquals(1, knownSuper.getSightings().size());
    }

    @Test
    public void testBuildCompleteOrganization() {
        Organization knownOrg = new Organization();
        knownOrg.setId(1);
        knownOrg.setName("Default Organization");
        knownOrg.setDescription("Default Description");
        knownOrg.setPhone("123-456-7890");
        knownOrg.setEmail("default@email.com");
        knownOrg = builder.buildCompleteOrganization(knownOrg);
        assertEquals(1, knownOrg.getLocations().size());
    }

    @Test
    public void testBuildCompleteSighting() {
        Sighting knownSighting = new Sighting();
        knownSighting.setId(1);
        knownSighting.setDateTime(Timestamp.valueOf(
                LocalDateTime.of(1000, Month.JANUARY, 01, 00, 00, 00)));
        knownSighting.setLocationId(1);
        knownSighting = builder.buildCompleteSighting(knownSighting);
        assertNotNull(knownSighting.getLocation());
        assertEquals(1, knownSighting.getSupers().size());
    }

}
