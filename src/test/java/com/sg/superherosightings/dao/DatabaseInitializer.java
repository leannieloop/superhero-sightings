package com.sg.superherosightings.dao;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

public class DatabaseInitializer {

    public static void setKnownGoodState() {

        String url = "jdbc:mysql://localhost:3306/HERO_DB_test";

        Properties properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "root");
        properties.put("serverTimezone", "UTC");

        List<String> statements = getStatements();

        try (Connection connection
                = DriverManager.getConnection(url, properties)) {
            Statement command = connection.createStatement();
            for (String sql : statements) {
                if (sql.trim().length() > 0) {
                    command.execute(sql);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error: " + e.getMessage(), e);
        }
    }

    private static List<String> getStatements() {

        URL scriptUrl = ClassLoader.getSystemResource("known-good-state.sql");

        List<String> statements = null;
        try {
            statements = Files.readAllLines(Paths.get(scriptUrl.getPath()));
        } catch (IOException e) {
            throw new RuntimeException("Error: " + e.getMessage(), e);
        }
        return statements;
    }
}
