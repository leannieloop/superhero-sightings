package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Location;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LocationDaoImplTest {

    private LocationDao dao;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        dao = ctx.getBean("locationDao", LocationDao.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testAddGetDeleteLocation() {
        Location location = new Location();
        location.setName("New Location");
        location.setCountry("NA");
        dao.addLocation(location);
        Integer locationId = location.getId();
        Location fromDb = dao.getLocationById(locationId);
        assertEquals(location, fromDb);
        dao.deleteLocation(locationId);
        assertNull(dao.getLocationById(locationId));
    }

    @Test
    public void testGetAllLocations() {
        List<Location> locations = dao.getAllLocations();
        assertNotEquals(0, locations.size());
    }

    @Test
    public void testGetLocationsWhereSuperHasBeenSighted() {
        List<Location> locationOfDefaultSuperSighting
                = dao.getLocationsWhereSuperHasBeenSighted(1);
        assertEquals(1, locationOfDefaultSuperSighting.size());
        assertEquals("Default Location",
                locationOfDefaultSuperSighting.get(0).getName());
    }

    @Test
    public void testGetLocationsByOrganization() {
        List<Location> locationOfDefaultOrganization
                = dao.getLocationsByOrganization(1);
        assertEquals(1, locationOfDefaultOrganization.size());
        assertEquals("Default Location",
                locationOfDefaultOrganization.get(0).getName());
    }

    @Test
    public void testAddUpdateDeleteLocation() {
        Location location = new Location();
        location.setName("Old Location");
        location.setCountry("NA");
        dao.addLocation(location);
        location.setName("New Location");
        dao.updateLocation(location);
        Integer locationId = location.getId();
        Location fromDb = dao.getLocationById(locationId);
        assertEquals(location, fromDb);
        dao.deleteLocation(locationId);
        assertNull(dao.getLocationById(locationId));
    }
}
