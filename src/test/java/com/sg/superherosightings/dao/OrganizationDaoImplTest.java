package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Organization;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class OrganizationDaoImplTest {

    private OrganizationDao dao;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        dao = ctx.getBean("organizationDao", OrganizationDao.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testAddGetDeleteOrganization() {
        Organization organization = new Organization();
        organization.setName("New Organization");
        organization.setDescription("New Description");
        organization.setPhone("123-456-7890");
        organization.setEmail("new@organization.com");
        dao.addOrganization(organization);
        Integer organizationId = organization.getId();
        Organization fromDb = dao.getOrganizationById(organizationId);
        assertEquals(organization, fromDb);
        dao.deleteOrganization(organizationId);
        assertNull(dao.getOrganizationById(organizationId));
    }

    @Test
    public void testGetAllOrganizations() {
        List<Organization> organizations = dao.getAllOrganizations();
        assertNotEquals(0, organizations.size());
    }

    @Test
    public void testGetOrganizationsBySuper() {
        List<Organization> organizationsForDefaultSuper
                = dao.getOrganizationsBySuper(1);
        assertEquals(1, organizationsForDefaultSuper.size());
        assertEquals("Default Organization",
                organizationsForDefaultSuper.get(0).getName());
    }

    @Test
    public void testAddUpdateDeleteOrganization() {
        Organization organization = new Organization();
        organization.setName("Old Org");
        organization.setDescription("New Description");
        organization.setPhone("123-456-7890");
        organization.setEmail("new@org.com");
        dao.addOrganization(organization);
        organization.setName("New Org");
        dao.updateOrganization(organization);
        Integer organizationId = organization.getId();
        Organization fromDb = dao.getOrganizationById(organizationId);
        assertEquals(organization, fromDb);
        dao.deleteOrganization(organizationId);
        assertNull(dao.getOrganizationById(organizationId));
    }
}
