package com.sg.superherosightings.dao;

import com.sg.superherosightings.model.Super;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SuperDaoImplTest {

    private SuperDao dao;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "test-applicationContext.xml");
        dao = ctx.getBean("superDao", SuperDao.class);
//        DatabaseInitializer.setKnownGoodState();
    }

    @Test
    public void testAddGetDeleteSuper() {
        Super newSuper = new Super();
        newSuper.setName("New Super");
        newSuper.setDescription("Alter Ego");
        dao.addSuper(newSuper);
        Integer superId = newSuper.getId();
        Super fromDb = dao.getSuperById(superId);
        assertEquals(newSuper, fromDb);
        dao.deleteSuper(superId);
        assertNull(dao.getSuperById(superId));
    }

    @Test
    public void testGetAllSupers() {
        List<Super> supers = dao.getAllSupers();
        assertNotEquals((int) 0, supers.size());
    }

    @Test
    public void testgetSupersBySightingLocation() {
        List<Super> supersAtDefaultLocation
                = dao.getSupersBySightingLocation(1);
        assertEquals(1, supersAtDefaultLocation.size());
        assertEquals("Default Super",
                supersAtDefaultLocation.get(0).getName());
    }

    @Test
    public void testGetSupersBySighting() {
        List<Super> supersAtDefaultSighting = dao.getSupersBySighting(1);
        assertEquals(1, supersAtDefaultSighting.size());
        assertEquals("Default Super", supersAtDefaultSighting.get(0).getName());
    }

    @Test
    public void testGetSupersByOrganization() {
        List<Super> supersInDefaultOrganization
                = dao.getSupersByOrganization(1);
        assertEquals(1, supersInDefaultOrganization.size());
        assertEquals("Default Super",
                supersInDefaultOrganization.get(0).getName());
    }

    @Test
    public void testAddUpdateDeleteSuper() {
        Super newSuper = new Super();
        newSuper.setName("Old Super");
        newSuper.setDescription("Alter Ego");
        dao.addSuper(newSuper);
        newSuper.setName("New Super");
        dao.updateSuper(newSuper);
        Integer superId = newSuper.getId();
        Super fromDb = dao.getSuperById(superId);
        assertEquals(newSuper, fromDb);
        dao.deleteSuper(superId);
        assertNull(dao.getSuperById(superId));
    }
}
